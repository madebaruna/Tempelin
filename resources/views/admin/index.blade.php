@extends('admin.layout')

@section('title', 'Beranda')
@section('title-header', 'Admin Dashboard')

@section('active-home', 'active')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="info-box">
                <span class="info-box-icon"><i class="fa fa-list-ol"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Jumlah Iklan Terverfikasi</span>
                    <span class="info-box-number">{{ $jumlah_iklan_verif }}</span>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="info-box">
                <span class="info-box-icon"><i class="fa fa-list-ul"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Jumlah Iklan Perlu Diverifikasi</span>
                    <span class="info-box-number">{{ $jumlah_iklan_unverif }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="info-box">
                <span class="info-box-icon"><i class="fa fa-user"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Jumlah User</span>
                    <span class="info-box-number">{{ $jumlah_user }}</span>
                </div>
            </div>
        </div>
    </div>
@endsection