@extends('layout')

@section('title', 'Pesan')

@section('content')
    <h2 class="kategori-title">Daftar Pesan</h2>
    <hr>
    <div class="pesan-box">
        <div class="row">
            <div class="col-md-4">
                <ul class="nav nav-pills nav-stacked">
                    @if(session('kirimbaru') != null)
                        <li role="presentation" style="border-bottom: 1px solid #eee;">
                            <a>
                                <span
                                    style="color: black !important;">Kirim pesan baru ke {{ \App\User::find(session('kirimbaru'))->name }}</span>
                            </a>
                        </li>
                    @endif
                    @foreach($messages as $m)
                        <li role="presentation" style="border-bottom: 1px solid #eee;">
                            @if(Auth::user()->id == $m->pengirim_id)
                                <a href="#" data-id="{{ $m->penerima_id }}" class="message-list">
                                    {{ \App\User::find($m->penerima_id)->name }}
                                </a>
                            @else
                                <a href="#" data-id="{{ $m->pengirim_id }}" class="message-list">
                                    {{ \App\User::find($m->pengirim_id)->name }}
                                </a>
                            @endif

                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-8" id="isiPesan">
                @if(session('current') != null)
                    <input type="hidden" id="pesan-identifier" value="{{ session('current') }}">
                @else
                    <h4>Klik pesan disamping untuk melihat isinya.</h4>
                @endif
            </div>
        </div>
    </div>
@endsection