<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {type} {nama} {email} {password} {ttl}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        User::create([
            'name' => $this->argument('nama'),
            'email' => $this->argument('email'),
            'password' => bcrypt($this->argument('password')),
            'tipe' => $this->argument('type'),
            'ttl' => $this->argument('ttl'),
        ]);
    }
}
