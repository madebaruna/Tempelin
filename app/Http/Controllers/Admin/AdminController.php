<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Iklan;
use App\User;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    public function index()
    {
        $jumlah_user = User::all()->count();
        $jumlah_iklan_verif = Iklan::where('isVerified', '1')->count();
        $jumlah_iklan_unverif = Iklan::where('isVerified', '0')->count();
        return view('admin.index', [
            'jumlah_user' => $jumlah_user,
            'jumlah_iklan_verif' => $jumlah_iklan_verif,
            'jumlah_iklan_unverif' => $jumlah_iklan_unverif
        ]);
    }
}
