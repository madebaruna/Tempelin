@extends('layout')

@section('title', 'Profile User')

@section('content')
    <h1 style="text-align: center;">Profile</h1>
    <hr>
    <div class="row">
        <div class="profile-container col-md-4">
            <div class="profile-userpic">
                <div class="picture"
                     style="background-image:url({{ $user->foto() }});">
                </div>
            </div>
            <p>
            <h4><i class="fa fa-user"></i> {{ $user->name }}</h4>
            <p class="text-justify">{!! nl2br(e($user->deskripsi)) !!}</p>
            <hr>
            <p><i class="fa fa-envelope"></i> {{ $user->email }}</p>
            <p><i class="fa fa-calendar"></i> {{ (new \Carbon\Carbon($user->ttl))->format('d F Y') }}</p>
            @if($edit)
                <a href="/profile/edit" class="btn btn-primary btn-lg edit-profile"
                   style="margin-top: 15px;"><i class="glyphicon glyphicon-edit"></i> Edit Profile</a>
            @endif
        </div>

        <div class="profile-container-iklan col-md-8">
            @foreach($iklan as $i)
                <div class="box">
                    <div class="gambar"
                         style="background-image:url({{ $i->gambarPertama->foto }});">
                    </div>
                    <div class="detail">
                        <a href="/iklan/{{ $user->id }}/{{ $i->slug }}"><h3>{{ $i->judul }}</h3></a>
                        <hr>
                        <i class="fa fa-map-marker"></i>
                        <a class="detail-minor" href="">{{ $i->kabupaten->kabupaten }}</a>,
                        <a class="detail-minor" href="">{{ $i->provinsi->provinsi }}</a>
                        <a class="detail-minor" href="">
                            <i style="margin-left: 10px" class="fa fa-list-ul"></i>
                            {{ $i->kategori->nama }}
                        </a>
                        <span class="detail-minor" href="">
                        <i style="margin-left: 10px" class="fa fa-clock-o"></i>
                            {{ (new \Carbon\Carbon($i->created_at))->format('d M Y H:m') }}
                        </span>
                        <span class="harga">{{ $i->harga() }}</span>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection('content')