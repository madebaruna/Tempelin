@extends('layout')

@section('title', 'Edit Profile')

@section('content')
    <h1 style="text-align: center;">Edit Profile</h1>
    <hr>
    <div class="box-edit">
        <div class="edit-profile-container">
            <div class="profile-userpic">
                <div class="picture"
                     style="background-image:url({{ Auth::user()->foto() }});">
                </div>
            </div>

            <form action="/profile/update" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <label class="upload-foto" for="uploadFotoProfil">
                    <i class="fa fa-camera" aria-hidden="true"></i>
                    <input type="file" id="uploadFotoProfil" name="foto">
                    <span class="hidden">imagename</span>
                </label>
                <div class="form-group form{{ $errors->has('name') ? ' has-error' : '' }}">
                    <input id="nama" type="text" class="form-control input-lg" name="name"
                           value="{{ Auth::user()->name }}" placeholder="Nama Anda">

                    @if ($errors->has('name'))
                        <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="form-control input-lg" name="email"
                           value="{{ Auth::user()->email }}" placeholder="E-mail">

                    @if ($errors->has('email'))
                        <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('tanggal_lahir') ? ' has-error' : '' }}">
                    <input id="ttl" type="text" class="form-control input-lg" name="tanggal_lahir"
                           value="{{ str_replace('-', '/', Auth::user()->ttl) }}" placeholder="Tanggal Lahir">

                    @if ($errors->has('tanggal_lahir'))
                        <span class="help-block">
                        <strong>{{ $errors->first('tanggal_lahir') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
            <textarea class="form-control" id="deskripsi" name="deskripsi" placeholder="Deskripsikan tentang anda..."
                      rows="5">{{ Auth::user()->deskripsi }}</textarea>
                    @if ($errors->has('deskripsi'))
                        <span class="help-block">
                        <strong>{{ $errors->first('deskripsi') }}</strong>
                </span>
                    @endif
                </div>
                <button class="btn btn-primary btn-lg edit-profile"><i class="glyphicon glyphicon-save"></i>
                    Simpan
                </button>
            </form>
        </div>
    </div>

@endsection