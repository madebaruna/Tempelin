<?php

namespace App\Http\Controllers;

use App\Category;
use App\Iklan;
use App\Kabupaten;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        $kabupaten = Kabupaten::orderBy('provinsi_id')->get();
        $kategori  = Category::where('parent_id', null)->orderBy('urutan')->get();
        $iklan     = Iklan::where('isVerified', 1)->orderBy('id', 'desc')->get();

        return view('index', [
            'kabupaten' => $kabupaten,
            'kategori'  => $kategori,
            'iklan'     => $iklan
        ]);
    }
}
