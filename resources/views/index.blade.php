@extends('layout')

@section('title', 'Beranda')

@section('content')
    <h2 class="kategori-title">Kategori Iklan</h2>
    <hr>
    <div class="kategori-home">
    @foreach($kategori as $k)
    <a href="{{ url("/$k->slug") }}" class="category-button">
        <i class="fa fa-{{ $k->icon }}"></i>
        <span>{{ $k->nama }}</span>
    </a>
    @endforeach
    </div><br/>
    <h2 class="kategori-title">Iklan Terbaru</h2>
    <hr>
    <div class="iklan-index">
        @foreach($iklan as $i)
            <div class="col-md-4">
                <a href="/iklan/{{ $i->user_id }}/{{ $i->slug }}">
                    <div class="card">
                        <div class="card-image" style="background-image: url({{ $i->gambarPertama->foto }})">
                        </div>
                        <div class="card-content">
                            <h5>{{ $i->judul }}</h5>
                            <a class="kategori" href="/{{$i->kategori->slug}}">{{ $i->kategori->nama }}</a><br/>
                            <hr>
                            <span class="harga">{{ $i->harga() }}</span>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
@endsection