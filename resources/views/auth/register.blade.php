@extends('auth.layout')

@section('title', 'Register')

@section('content')
    <div class="box">
    <a href="/"><img class="logo" src="{{ asset('/img/Logo-small.png') }}" alt="Logo"></a>
        <hr>
        <h1>Register</h1>
        <form role="form" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
            <div class="form-group form{{ $errors->has('name') ? ' has-error' : '' }}">
                <input id="nama" type="text" class="form-control input-lg" name="name"
                       value="{{ old('name') }}"
                       required autofocus placeholder="Nama Anda">

                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input id="email" type="email" class="form-control input-lg" name="email" value="{{ old('email') }}"
                       required autofocus placeholder="E-email">

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="password" type="password" class="form-control input-lg" name="password" required
                       placeholder="Password">
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="password" type="password" class="form-control input-lg" name="password_confirmation" required
                       placeholder="Konfirmasi password">

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('tanggal_lahir') ? ' has-error' : '' }}">
                <input id="ttl" type="text" class="form-control input-lg" name="tanggal_lahir" required
                       placeholder="Tanggal Lahir">

                @if ($errors->has('tanggal_lahir'))
                    <span class="help-block">
                        <strong>{{ $errors->first('tanggal_lahir') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg">
                    Register
                </button>
            </div>
        </form>
    </div>

    <h4>Sudah punya akun? <a class="register" href="{{ route('login') }}">Login</a></h4>
@endsection
