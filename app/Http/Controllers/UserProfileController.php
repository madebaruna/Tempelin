<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserProfileController extends Controller
{
    public function profile($id = null)
    {
        $edit = false;

        if ($id == null) {
            $user = Auth::user();
            $edit = true;
        } else
            $user = User::findOrFail($id);

        $iklan = $user->iklan()->where('isVerified', 1)->paginate();

        return view('user.profile', [
            'iklan' => $iklan,
            'user'  => $user,
            'edit'  => $edit
        ]);

        return view('user.profile');
    }

    public function edit()
    {
        return view('user.edit');
    }

    public function update(Request $request)
    {
        $user       = Auth::user();
        $validation = [
            'name'          => 'required|string|max:191',
            'tanggal_lahir' => 'required|string',
            'deskripsi'     => 'string'
        ];

        $data = $request->all();

        if ($user->email != $data['email']) {
            $validation['email'] = 'required|string|email|max:191|unique:users';
        }

        if ($user->foto != null) {
            $validation['foto'] = 'image|max:2000';
        }

        $this->validate($request, $validation);
        $user->name      = $data['name'];
        $user->email     = $data['email'];
        $user->ttl       = Carbon::createFromFormat("d/m/Y", $data['tanggal_lahir'])->format('Y-m-d');
        $user->deskripsi = $data['deskripsi'];
        if ($request->hasFile('foto')) {
            $files      = $request->file('foto');
            $path       = $files->store('profile', 'uploads');
            $user->foto = $path;
        }
        $user->save();

        return redirect('/profile');
    }
}
