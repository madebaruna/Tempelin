@extends('admin.layout')

@section('title', 'Detail Iklan')
@section('title-header', 'Iklan: ' . $iklan->judul)

@section('active-iklan', 'active')

@section('content')
    @if(!$iklan->isVerified)
    <a href="/admin/iklan/verifikasi/{{ $iklan->id }}" class="btn btn-lg btn-primary">
        <i class="fa fa-check"></i> Verifikasi
    </a>
    @endif
    <a href="/admin/iklan/delete/{{ $iklan->id }}" class="btn btn-lg btn-danger">
        <i class="fa fa-trash"></i> Hapus
    </a>
    <br>
    <br>
    <table class="table table-bordered">
        <tr>
            <td class="fit">Iklan</td>
            <td>{{ $iklan->judul }}</td>
        </tr>
        <tr>
            <td class="fit">User</td>
            <td>{{ $iklan->user->name }}</td>
        </tr>
        <tr>
            <td class="fit">No Tlp</td>
            <td>{{ $iklan->nomor_telepon }}</td>
        </tr>
        <tr>
            <td class="fit">Lokasi</td>
            <td>{{ $iklan->provinsi->provinsi }},
                {{ $iklan->kabupaten->tipe == 'Kota' ? 'Kota' : '' }}
                {{ $iklan->kabupaten->kabupaten }}</td>
        </tr>
        <tr>
            <td class="fit">Harga</td>
            <td>{{ $iklan->harga }}</td>
        </tr>
        <tr>
            <td class="fit">Deskripsi</td>
            <td>{!! nl2br(e($iklan->deskripsi)) !!}</td>
        </tr>
        @foreach($gambar as $k => $g)
        <tr>
            <td class="fit">Gambar {{ $k+1 }}</td>
            <td><img class="gambar-iklan selected" src="{{$g->foto}}" alt=""></td>
        </tr>
        @endforeach
    </table>
@endsection