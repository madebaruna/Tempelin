<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Helpers\NotificationHelper;
use App\Iklan;
use App\Kabupaten;
use App\Notification;
use App\Pesan;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function read(Request $request, $id)
    {
        $notification = Notification::findOrFail($id);
        NotificationHelper::read($notification);

        if ($notification->comment_id != null) {
            $iklan = Comment::find($notification->comment_id)->iklan;

            return redirect('/iklan/' . $iklan->user_id . '/' . $iklan->slug);
        } else {
            $pesan = Pesan::find($notification->pesan_id);
            $request->session()->flash('current', $pesan->pengirim_id);
            return redirect('/pesan/');
        }
    }
}
