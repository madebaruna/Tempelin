@extends('layout')

@section('title', 'Tempel Iklan')

@section('content')
    <h2 class="kategori-title">Tempel Iklan Baru</h2>
    <hr>
    <form method="POST" action="{{ url('/tempel') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
            <label for="judul">Judul Iklan</label>
            <input type="text" class="form-control input-lg" id="judul" name="judul"
                   placeholder="Judul Iklan" value="{{ old('judul') }}">
            @if ($errors->has('judul'))
                <span class="help-block">
                        <strong>{{ $errors->first('judul') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('kategori') ? ' has-error' : '' }}">
            <label for="kategori">Kategori</label><br/>
            <input type="hidden" class="category-selection" name="kategori" value="{{ old('kategori') }}">
            <span class="category-selection-text hidden"></span>
            <button class="btn btn-default pilih-kategori-btn btn-lg"
                    data-toggle="modal"
                    data-target=".pilih-kategori"
                    type="button">
                Pilih kategori
            </button>
            @if ($errors->has('kategori'))
                <span class="help-block">
                        <strong>{{ $errors->first('kategori') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('harga') ? ' has-error' : '' }}">
            <label for="harga">Harga</label>
            <div class="input-group input-group-lg">
                <span class="input-group-addon" id="harga-addon">Rp</span>
                <input id="harga" type="number" name="harga" class="form-control" placeholder="0"
                       aria-describedby="harga-addon" value="{{ old('harga') }}">
            </div>
            @if ($errors->has('harga'))
                <span class="help-block">
                        <strong>{{ $errors->first('harga') }}</strong>
                </span>
            @endif
        </div>
        <label for="judul">Gambar</label><br/>
        <div class="row">
            <div class="form-group upload-gambar-form
            {{ $errors->has('gambar.0')||$errors->has('gambar.1')||$errors->has('gambar.2')
                ||$errors->has('gambar.3')||$errors->has('gambar.4')||$errors->has('gambar.5') ? ' has-error' : '' }}"">
                <div class="col-md-4">
                    <label for="upload-gambar-1" class="upload-gambar">
                        <i class="fa fa-times hidden"></i>
                        <i class="fa fa-picture-o"></i> <span>Pilih Gambar</span>
                        <input accept="image/*" class="upload-gambar-input" id="upload-gambar-1" type="file"
                               name="gambar[]">
                    </label>
                </div>
                <div class="col-md-4">
                    <label for="upload-gambar-2" class="upload-gambar">
                        <i class="fa fa-times hidden"></i>
                        <i class="fa fa-picture-o"></i> <span>Pilih Gambar</span>
                        <input accept="image/*" class="upload-gambar-input" id="upload-gambar-2" type="file"
                               name="gambar[]">
                    </label>
                </div>
                <div class="col-md-4">
                    <label for="upload-gambar-3" class="upload-gambar">
                        <i class="fa fa-times hidden"></i>
                        <i class="fa fa-picture-o"></i> <span>Pilih Gambar</span>
                        <input accept="image/*" class="upload-gambar-input" id="upload-gambar-3" type="file"
                               name="gambar[]">
                    </label>
                </div>
                <div class="col-md-4">
                    <label for="upload-gambar-4" class="upload-gambar">
                        <i class="fa fa-times hidden"></i>
                        <i class="fa fa-picture-o"></i> <span>Pilih Gambar</span>
                        <input accept="image/*" class="upload-gambar-input" id="upload-gambar-4" type="file"
                               name="gambar[]">
                    </label>
                </div>
                <div class="col-md-4">
                    <label for="upload-gambar-5" class="upload-gambar">
                        <i class="fa fa-times hidden"></i>
                        <i class="fa fa-picture-o"></i> <span>Pilih Gambar</span>
                        <input accept="image/*" class="upload-gambar-input" id="upload-gambar-5" type="file"
                               name="gambar[]">
                    </label>
                </div>
                <div class="col-md-4">
                    <label for="upload-gambar-6" class="upload-gambar">
                        <i class="fa fa-times hidden"></i>
                        <i class="fa fa-picture-o"></i> <span>Pilih Gambar</span>
                        <input accept="image/*" class="upload-gambar-input" id="upload-gambar-6" type="file"
                               name="gambar[]">
                    </label>
                </div>
                @if ($errors->has('gambar.0')||$errors->has('gambar.1')||$errors->has('gambar.2')
                ||$errors->has('gambar.3')||$errors->has('gambar.4')||$errors->has('gambar.5'))
                    <span class="help-block" style="padding: 20px;">
                        <strong>{{ $errors->first('gambar.0') }}</strong>
                        <strong>{{ $errors->first('gambar.1') }}</strong>
                        <strong>{{ $errors->first('gambar.2') }}</strong>
                        <strong>{{ $errors->first('gambar.3') }}</strong>
                        <strong>{{ $errors->first('gambar.4') }}</strong>
                        <strong>{{ $errors->first('gambar.5') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
            <label for="judul">Deskripsi Iklan</label>
            <textarea class="form-control" id="deskripsi" name="deskripsi" placeholder="Deskripsikan iklan anda..."
                      rows="5">{{ old('deskripsi') }}</textarea>
            @if ($errors->has('deskripsi'))
                <span class="help-block">
                        <strong>{{ $errors->first('deskripsi') }}</strong>
                </span>
            @endif
        </div>
        <hr>
        <div class="form-group{{ $errors->has('nomor_telepon') ? ' has-error' : '' }}">
            <label for="nomor_telepon">Nomor Telepon</label>
            <input type="text" class="form-control input-lg" id="nomor_telepon" name="nomor_telepon"
                   placeholder="Nomor Telepon" value="{{ old('nomor_telepon') }}">
            @if ($errors->has('nomor_telepon'))
                <span class="help-block">
                        <strong>{{ $errors->first('nomor_telepon') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
            <label for="alamat">Alamat</label>
            <input type="text" class="form-control input-lg" id="alamat" name="alamat"
                   placeholder="Alamat" value="{{ old('alamat') }}">
            @if ($errors->has('alamat'))
                <span class="help-block">
                        <strong>{{ $errors->first('alamat') }}</strong>
                </span>
            @endif
        </div>
        <input type="hidden" class="kabupaten-json" value="{{ $kabupaten }}">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group{{ $errors->has('provinsi') ? ' has-error' : '' }}">
                    <label for="judul">Provinsi</label>
                    <select class="form-control input-lg provinsi-select" name="provinsi">
                        <option selected value="" disabled>Pilih provinsi</option>
                        @foreach($provinsi as $p)
                            <option {{ $p->id == old('provinsi') ? 'selected' : '' }} value="{{ $p->id }}">{{ $p->provinsi }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('provinsi'))
                        <span class="help-block">
                        <strong>{{ $errors->first('provinsi') }}</strong>
                </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group{{ $errors->has('kabupaten') ? ' has-error' : '' }}">
                    <label for="judul">Kabupaten</label>
                    <select class="form-control input-lg kabupaten-select" name="kabupaten">
                        <option selected value="" disabled>Pilih provinsi dahulu</option>
                    </select>
                    @if ($errors->has('kabupaten'))
                        <span class="help-block">
                        <strong>{{ $errors->first('kabupaten') }}</strong>
                </span>
                    @endif
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary btn-lg" style="width: 100%">
            TEMPEL <i class="fa fa-thumb-tack"></i>
        </button>
    </form>
    @include('pilih_kategori_modal')
@endsection