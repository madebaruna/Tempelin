<div class="modal fade pilih-kabupaten-search" tabindex="-1" role="dialog" aria-labelledby="pilih-kabupaten">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Kabupaten</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    @php
                        $provinsi = \App\Provinsi::all();
                        $kabupaten = \App\Kabupaten::all();
                    @endphp
                    <table class="table provinsi">
                        @foreach($provinsi as $i => $p)
                            @if($i%4==0)
                                <tr>@endif
                                    <td class="provinsi"><a href="#" data-id="{{ $p->id }}">{{ $p->provinsi }}</a></td>
                                    @if(($i+1)%4==0)</tr>@endif
                        @endforeach
                    </table>
                    <table class="table kabupaten">
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <input type="hidden" class="kabupaten-json" value="{{ json_encode($kabupaten) }}">
            </div>
        </div>
    </div>
</div>
</div>
