@extends('layout')

@section('title', 'Profile User')

@section('content')
    <h1 style="text-align: center;">Daftar Iklan</h1>
    <hr>
    <div class="profile-container-iklan small">
        @foreach($iklan as $i)
            <div class="box">
                <div class="gambar"
                     style="background-image:url({{ $i->gambarPertama->foto }});">
                </div>
                <div class="detail">
                    <a href="/iklan/{{ $user->id }}/{{ $i->slug }}"><h3>{{ $i->judul }}</h3></a>
                    @if($i->isVerified == 0)
                        <span class="verifikasi">Belum Diverifikasi</span>
                    @endif
                    <hr>
                    <i class="fa fa-map-marker"></i>
                    <a class="detail-minor" href="">{{ $i->kabupaten->kabupaten }}</a>,
                    <a class="detail-minor" href="">{{ $i->provinsi->provinsi }}</a>
                    <a class="detail-minor" href="">
                        <i style="margin-left: 10px" class="fa fa-list-ul"></i>
                        {{ $i->kategori->nama }}
                    </a>
                    <span class="detail-minor" href="">
                        <i style="margin-left: 10px" class="fa fa-clock-o"></i>
                        {{ (new \Carbon\Carbon($i->created_at))->format('d M Y H:m') }}
                    </span>
                    <span class="harga">{{ $i->harga() }}</span>
                </div>
            </div>
        @endforeach
    </div>
    {{ $iklan->links() }}

@endsection('content')