@extends('admin.layout')

@section('title', 'Kategori')
@section('title-header', 'Daftar Kategori')

@section('active-kategori', 'active')

@section('content')
    <div class="row">
        <div class="col-md-7">
            <h3>Drag & Drop untuk mengubah urutan kategori</h3>
            <ul class="kategori">
                @foreach($kategori as $k)
                    <li class="parent-kategori" data-id="{{ $k['id'] }}">
                        <button data-id="{{ $k['id'] }}" class="btn btn-xs btn-default hapus-kategori-btn"
                                data-toggle="modal"
                                data-target=".hapus-kategori">
                            <i class="fa fa-trash"></i>
                        </button>
                        {{ $k['nama'] }}
                        <ul class="sub-kategori">
                            @foreach($k['sub_category'] as $s)
                                <li class="sub-kategori" data-id="{{ $s['id'] }}">
                                    <button data-id="{{ $s['id'] }}" class="btn btn-xs btn-default hapus-kategori-btn"
                                            data-toggle="modal"
                                            data-target=".hapus-kategori">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    {{ $s['nama'] }}
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endforeach
            </ul>
            <form method="POST" id="kategori-urutan" action="/admin/kategori/urut">
                {{ csrf_field() }}
                <input type="hidden" class="urutan" name="urutan">
                <button class="btn btn-primary">Ubah Urutan</button>
            </form>
        </div>
        <div class="col-md-5">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Tambah Kategori</h3>
                </div>
                <div class="box-content">
                    <form method="POST" action="{{ url('/admin/kategori/add') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="nama">Nama Kategori</label>
                            <input type="text" class="form-control" id="nama" name="nama"
                                   placeholder="Nama Kategori">
                        </div>
                        <div class="form-group">
                            <label for="parent">Sub Kategori Dari</label>
                            <select class="form-control kategori-parent" id="parent" name="parent">
                                <option value="0">Tidak Ada</option>
                                @foreach($kategori as $k)
                                    <option value="{{ $k['id'] }}">{{ $k['nama'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="icon">Icon</label>
                            <input type="text" class="form-control" id="icon" name="icon"
                                   placeholder="Nama Icon">
                        </div>
                        <button type="submit" class="btn btn-default">Tambah</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade hapus-kategori" tabindex="-1" role="dialog" aria-labelledby="hapusKategori">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Hapus kategori ini?</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Apakah anda yakin ingin menghapus kategori ini?<br/>
                        Semua iklan dengan kategori ini juga akan terhapus!!<br/>c
                        Dan semua subkategori juga akan terhapus!!
                    </p>
                </div>
                <div class="modal-footer">
                    <form data-url="{{ url('/admin/kategori/delete/') }}" method="POST" action="">
                        {{ csrf_field() }}
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>
                            Batal
                        </button>
                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection