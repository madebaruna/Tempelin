@extends('layout')

@section('title', 'Pencarian ' . $search)

@section('content')
    <h2 class="kategori-title">Hasil pencarian {{ $search }}</h2>
    <div class="detail-iklan">
        <i class="fa fa-map-marker"></i>
        {{ $kabupaten == null ? 'Semua Kabupaten' : $kabupaten->kabupaten }},
        {{ $provinsi == null ? 'Semua Provinsi' : $provinsi->provinsi }}

        <i style="display: inline-block; margin-left: 10px;" class="fa fa-list-ul"></i>
        {{ $kategori == null ? 'Semua Kategori' : $kategori->nama }}
    </div>
    <hr>
    <div class="row">
        @foreach($iklan as $i)
            <div class="col-md-4">
                <a href="/iklan/{{ $i->user_id }}/{{ $i->slug }}">
                    <div class="card">
                        <div class="card-image" style="background-image: url({{ $i->gambarPertama->foto }})">
                        </div>
                        <div class="card-content">
                            <h5>{{ $i->judul }}</h5>
                            <a class="kategori" href="/{{$i->kategori->slug}}">{{ $i->kategori->nama }}</a><br/>
                            <hr>
                            <span class="harga">{{ $i->harga() }}</span>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
@endsection