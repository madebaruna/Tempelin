<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title') - Admin {{ config('app.name') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Cabin:400,600|Raleway" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/admin.css') }}">
</head>
<body>
<div class="container">
    <header>
        <a href="/"><img src="{{ asset('/img/logo-small.png') }}"></a>
        <div class="tombol-header pull-right">
            <p class="header-message">Tempel iklanmu disini!</p>
            @if(!Auth::user())
                <a href="/login" class="btn btn-default btn-lg"><i class="fa fa-sign-in"></i> Login</a>
            @else
                <a href="/login" class="btn btn-default btn-lg"><i class="fa fa-bell-o"></i></a>
                <div class="dropdown" style="display:inline-block;">
                    <button class="btn btn-default btn-lg dropdown-toggle" type="button" id="headerDropdown"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        {{ Auth::user()->name }}
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="headerDropdown">
                        @if(Auth::user()->isAdmin())
                        <li><a href="/admin"><i class="fa fa-dashboard"></i> Admin Dashboard</a></li>
                        @endif()
                        <li><a href="/profile"><i class="fa fa-user"></i> Profil</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/logout"><i class="fa fa-sign-out"></i> Logout</a></li>
                    </ul>
                </div>
            @endif
            <a href="/tempel" class="btn btn-primary btn-lg"><i class="fa fa-thumb-tack"></i> Tempel Iklan</a>
        </div>
    </header>
    <section>
        <h2 class="kategori-title">@yield('title-header')</h2>
        <hr>
        <div class="row">
            <div class="col-md-3 admin-menu">
                <ul class="nav nav-pills nav-stacked">
                    <li role="presentation" class="@yield('active-home')"><a href="/admin">Home</a></li>
                    <li role="presentation" class="@yield('active-iklan')">
                        <a href="/admin/iklan">Iklan
                        @php($icount = \App\Iklan::where('isVerified', '0')->count())
                        @if($icount > 0)
                            <span class="badge">{{ $icount }}</span>
                        @endif
                        </a>
                    </li>
                    <li role="presentation" class="@yield('active-user')"><a href="/admin/user">User</a></li>
                    <li role="presentation" class="@yield('active-kategori')"><a href="/admin/kategori">Kategori</a></li>
                </ul>
            </div>
            <div class="col-md-9 admin-content">
                @yield('content')
            </div>
        </div>
    </section>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <a href="">Pusat Bantuan</a>
                <a href="">Syarat & Ketentuan</a>
                <a href="">Tentang Tempelin</a>
                <a href="">Tips Belanja Aman</a>
                <span>Copyright 2017 Tempel.in</span>
            </div>
            <div class="col-md-6" style="text-align: left;">
                <img src="{{ asset('/img/logo-invert.png') }}">
            </div>
        </div>
    </div>
</footer>
<script src="{{ asset('/js/jquery.min.js') }}"></script>
<script src="{{ asset('/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/select2.min.js') }}"></script>
<script src="{{ asset('/js/admin.js') }}"></script>
</body>
</html>