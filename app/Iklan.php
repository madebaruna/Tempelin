<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iklan extends Model
{
    protected $table = 'iklan';
    protected $fillable = [
        'kategori_id',
        'user_id',
        'judul',
        'deskripsi',
        'slug',
        'harga',
        'isVerified',
        'status',
        'nomor_telepon',
        'alamat',
        'provinsi_id',
        'kabupaten_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function gambar()
    {
        return $this->hasMany(Foto::class);
    }

    public function kategori()
    {
        return $this->belongsTo(Category::class);
    }

    public function gambarPertama()
    {
        return $this->hasOne(Foto::class);
    }

    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class);
    }

    public function kabupaten()
    {
        return $this->belongsTo(Kabupaten::class);
    }

    public function komentar()
    {
        return $this->hasMany(Comment::class);
    }

    public function verify()
    {
        $this->isVerified = 1;
        $this->save();
    }

    public function harga()
    {
        return 'Rp ' . number_format($this->harga, 0, ",", ".");
    }
}
