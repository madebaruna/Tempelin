<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'nama', 'slug', 'parent_id', 'urutan', 'icon'
    ];

    public static function getCategory()
    {
        $category = [];
        $categories = Category::where('parent_id', NULL)->orderBy('urutan')->get();
        foreach ($categories as $c) {
            $cat = [
                'id' => $c->id,
                'nama' => $c->nama,
                'icon' => $c->icon,
                'sub_category' => []
            ];

            $subs = Category::where('parent_id', $c->id)->orderBy('urutan')->get();
            foreach ($subs as $s) {
                $sub = [
                    'id' => $s->id,
                    'nama' => $s->nama,
                    'icon' => $s->icon
                ];

                array_push($cat['sub_category'], $sub);
            }

            array_push($category, $cat);
        }

        return $category;
    }
}
