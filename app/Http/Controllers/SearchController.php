<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Helpers\NotificationHelper;
use App\Iklan;
use App\Kabupaten;
use App\Notification;
use App\Provinsi;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search($prov, $kab, $kat, $search = '')
    {
        $kategori  = Category::where('slug', $kat)->first();
        $provinsi  = Provinsi::find($prov);
        $kabupaten = Kabupaten::find($kab);

        $iklan = Iklan::where('judul', 'LIKE', "%$search%");
        if ($kat != 'semua') $iklan == $iklan->where('kategori_id', $kategori->id);
        if ($prov != -1) $iklan == $iklan->where('provinsi_id', $prov);
        if ($kab != -1) $iklan == $iklan->where('kabupaten_id', $kab);
        $iklan = $iklan->where('isVerified', 1)->paginate();

        return view('iklan.daftar_iklan_search', [
            'search'    => $search,
            'iklan'     => $iklan,
            'kategori'  => $kategori,
            'provinsi'  => $provinsi,
            'kabupaten' => $kabupaten
        ]);
    }

    public function searchRedirect(Request $request)
    {
        $data     = $request->all();
        $kategori = 'semua';
        if ($data['kategori'] != null)
            $kategori = Category::findOrFail($data['kategori'])->slug;

        return redirect("/cari/$data[provinsi]/$data[kabupaten]/$kategori/$data[search]");
    }
}
