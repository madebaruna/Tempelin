<?php

namespace App\Http\Controllers;

use App\Category;
use App\Iklan;
use App\Kabupaten;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IklanController extends Controller
{
    public function get()
    {
        $user  = Auth::user();
        $iklan = $user->iklan()->paginate();

        return view('user.daftar_iklan', [
            'iklan' => $iklan,
            'user'  => $user
        ]);
    }

    public function detail($user_id, $slug)
    {
        $user     = User::findOrFail($user_id);
        $iklan    = $user->iklan()->where('slug', $slug)->firstOrFail();
        $komentar = $iklan->komentar;

        if ($iklan->isVerified == 0)
            return abort(404);

        return view('iklan.detail_iklan', [
            'iklan'    => $iklan,
            'user'     => $user,
            'komentar' => $komentar
        ]);
    }

    public function listing($slug)
    {
        $kategori = Category::where('slug', $slug)->firstOrFail();
        $iklan    = Iklan::whereHas('kategori', function ($query) use (&$kategori) {
            $query->where('parent_id', $kategori->id);
        })->orWhere('kategori_id', $kategori->id)->where('isVerified', 1)->get();

        return view('iklan.daftar_iklan', [
            'iklan'    => $iklan,
            'kategori' => $kategori
        ]);
    }

    public function lokasi($id)
    {
        $kabupaten = Kabupaten::findOrFail($id);
        $iklan     = Iklan::where('isVerified', 1)->where('kabupaten_id', $id)->get();

        return view('iklan.daftar_iklan_by_location', [
            'iklan'     => $iklan,
            'kabupaten' => $kabupaten
        ]);
    }
}
