$(function () {
    $(".search-kabupaten").select2({
        placeholder: 'Kabupaten'
    });
    $(".search-kategori").select2({
        placeholder: 'Kategori'
    });

    if ($('.pilih-kategori a.main-category').length) {
        var category = JSON.parse($('.category-data').val());
    }

    if ($('.pilih-kategori-search a.main-category').length) {
        var category = JSON.parse($('.category-data').val());
    }

    $('.pilih-kategori a.main-category').click(function (e) {
        var id = $(this).data('id');
        var index = -1;
        for (var i = 0; i < category.length; i++) {
            if (category[i].id == id) {
                index = i;
                break;
            }
        }

        var icon = category[index]['icon'];
        var nama = category[index]['nama'];

        $('.main-category-li').removeClass('active');
        $(this).parent().addClass('active');
        $('.main-category-icon').attr('class', 'main-category-icon fa fa-' + icon);
        $('.main-category-name').text(nama);
        $('.sub-category-box').css('border-left', '3px solid #1abc9c');

        var subCategoryList = $('.sub-category-list');
        subCategoryList.html('');

        var sub = category[index]['sub_category'];
        for (var i = 0; i < sub.length; i++) {
            c = sub[i];
            subCategoryList.append('<li role="presentation" class="main-category-li">' +
                '<a class="sub-category" data-id="' + c['id'] + '" href="#">' +
                '<i class="fa fa-' + c['icon'] + '"></i> ' + c['nama'] + '</a>' +
                '</li>  ');
        }

        e.preventDefault();
    });

    $('.sub-category-list').on('click', '.sub-category', function (e) {
        $('.category-selection-text').text($(this).text());
        $('.category-selection-text').removeClass('hidden');
        $('.category-selection').val($(this).data('id'));
        $('.pilih-kategori-btn').text('Ubah');
        $('.pilih-kategori').modal('hide')

        e.preventDefault();
    });

    $('.pilih-kategori-search a.main-category').click(function (e) {
        var id = $(this).data('id');
        var index = -1;
        for (var i = 0; i < category.length; i++) {
            if (category[i].id == id) {
                index = i;
                break;
            }
        }

        var icon = category[index]['icon'];
        var nama = category[index]['nama'];

        $('.main-category-li').removeClass('active');
        $(this).parent().addClass('active');
        $('.main-category-icon').attr('class', 'main-category-icon fa fa-' + icon);
        $('.main-category-name').text(nama);
        $('.sub-category-box').css('border-left', '3px solid #1abc9c');

        var subCategoryList = $('.sub-category-list-search');
        subCategoryList.html('');

        var sub = category[index]['sub_category'];
        for (var i = 0; i < sub.length; i++) {
            c = sub[i];
            subCategoryList.append('<li role="presentation" class="main-category-li">' +
                '<a class="sub-category-search" data-id="' + c['id'] + '" href="#">' +
                '<i class="fa fa-' + c['icon'] + '"></i> ' + c['nama'] + '</a>' +
                '</li>  ');
        }

        e.preventDefault();
    });

    $('.sub-category-list-search').on('click', '.sub-category-search', function (e) {
        $('.kategori-id-search').val($(this).data('id'));
        $('.kategori-input-search').val($(this).text());
        $('.pilih-kategori-search').modal('hide');

        e.preventDefault();
    });

    $('.upload-foto').on('change', '#uploadFotoProfil', function () {
        var file = $(this)[0].files[0];
        if (file) {
            $(this).siblings('span').text(file.name);
            $(this).siblings('span').removeClass('hidden');
        } else {
            $(this).siblings('span').addClass('hidden');
        }
    });

    $('.upload-gambar-form').on('change', '.upload-gambar-input', function () {
        var file = $(this)[0].files[0];
        if (file) {
            $(this).siblings('span').text(file.name);
            $(this).siblings('.fa-times').removeClass('hidden');
            $(this).siblings('.fa-picture-o').addClass('hidden');
        } else {
            $(this).siblings('span').text('Pilih Gambar');
            $(this).siblings('.fa-times').addClass('hidden');
            $(this).siblings('.fa-picture-o').removeClass('hidden');
            $(this).siblings('.upload-gambar-input').val('');
        }
    });

    $('.upload-gambar').on('click', '.fa-times', function (event) {
        $(this).siblings('span').text('Pilih Gambar');
        $(this).addClass('hidden');
        $(this).siblings('.fa-picture-o').removeClass('hidden');
        $(this).siblings('.upload-gambar-input').val('');
        event.preventDefault();
    });

    if ($('.kabupaten-json').length) {
        var kabupaten = JSON.parse($('.kabupaten-json').val());
    }

    $('.provinsi-select').change(function () {
        var provinsi_id = $(this).val();
        var kabupaten_select = $('.kabupaten-select');
        kabupaten_select.html('');
        for (var i = 0; i < kabupaten.length; i++) {
            if (kabupaten[i].provinsi_id == provinsi_id) {
                kabupaten_select.append('<option value="'
                    + kabupaten[i].id + '">'
                    + (kabupaten[i].tipe === 'Kota' ? 'Kota' : '') + ' '
                    + kabupaten[i].kabupaten
                    + '</option>')
            }
        }
    });

    $('.pilih-kabupaten-search').on('click', 'td.provinsi a', function (e) {
        $('table.provinsi').hide();
        var provinsi_id = $(this).data('id');
        $('.provinsi-id-search').val(provinsi_id);
        var table = $('table.kabupaten tbody');
        table.html('');
        var counter = 0;
        var rows = '<tr><td><a href="#" class="kabupaten-back"><i class="fa fa-arrow-left"></i> Kembali</a></td><tr>';
        for (var i = 0; i < kabupaten.length; i++) {
            if (kabupaten[i].provinsi_id == provinsi_id) {
                if (counter % 4 == 0) rows += '<tr>';
                rows += '<td class="kabupaten"><a href="#" ' +
                    'data-id="' + kabupaten[i].id +
                    '">' + (kabupaten[i].tipe == 'Kota' ? 'Kota ' : '') +
                    kabupaten[i].kabupaten + '</a></td>';
                if ((counter + 1) % 4 == 0) rows += '</tr>';
                counter++;
            }
        }
        rows += '<tr><td class="kabupaten"><a href="#" data-id="-1">Semua di ' +
            $(this).text() + '</a></td><tr>';
        table.append(rows);

        e.preventDefault();
    });

    $('.pilih-kabupaten-search').on('click', '.kabupaten-back', function (e) {
        $('table.kabupaten tbody').html('');
        $('table.provinsi').show();

        e.preventDefault();
    });

    $('.pilih-kabupaten-search').on('click', 'td.kabupaten a', function (e) {
        $('.kabupaten-id-search').val($(this).data('id'));
        $('.kabupaten-input-search').val($(this).text());
        $('.pilih-kabupaten-search').modal('hide');

        e.preventDefault();
    });

    //Slide Gambar
    $('.iklan-left-col .thumb img').click(function () {
        var newImg = $(this).attr('src');

        $('.iklan-left-col .thumb img').removeClass('selected');
        $(this).addClass('selected');

        $("#gambar-iklan").fadeOut(100, function () {
            $("#gambar-iklan").attr("src", newImg);
            $("#gambar-iklan-link").attr("href", newImg);
        }).fadeIn(200);
        return false;
    });

    if ($('#pesan-identifier').length) {
        console.log('/pesan/' + $('#pesan-identifier').val());
        $.get('/pesan/' + $('#pesan-identifier').val(), function(res) {
            $('#isiPesan').html(res);
        });
    }

    $('.message-list').click(function(e){
        $.get('/pesan/' + $(this).data('id'), function(res) {
            $('#isiPesan').html(res);
        });
        e.preventDefault();
    });
});