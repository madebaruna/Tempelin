<?php

namespace App\Http\Controllers;

use App\Foto;
use App\Iklan;
use App\Kabupaten;
use App\Provinsi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FormIklanController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function laku($id)
    {
        $iklan = Auth::user()->iklan()->find($id);
        $iklan->status = 'laku';
        $iklan->save();
        return redirect()->back();
    }

    public function add()
    {
        $provinsi  = Provinsi::all();
        $kabupaten = json_encode(Kabupaten::all());

        return view('iklan.tambah_iklan', [
            'provinsi'  => $provinsi,
            'kabupaten' => $kabupaten
        ]);
    }

    public function post(Request $request)
    {
        $this->validate($request, [
            'kategori'      => 'required|exists:categories,id',
            'judul'         => 'required|string|max:191',
            'deskripsi'     => 'required|string',
            'harga'         => 'required|numeric',
            'nomor_telepon' => 'required|string|max:191',
            'alamat'        => 'required|string|max:191',
            'provinsi'      => 'required|exists:provinsi,id',
            'kabupaten'     => 'required|exists:kabupaten,id',
            'gambar.*'      => 'image|max:2000'
        ]);

        $user = Auth::user();
        $data = $request->all();

        $iklan = Iklan::create([
            'kategori_id'   => $data['kategori'],
            'user_id'       => $user->id,
            'judul'         => $data['judul'],
            'deskripsi'     => $data['deskripsi'],
            'slug'          => str_slug($data['judul']),
            'harga'         => $data['harga'],
            'isVerified'    => 0,
            'status'        => '',
            'nomor_telepon' => $data['nomor_telepon'],
            'alamat'        => $data['alamat'],
            'provinsi_id'   => $data['provinsi'],
            'kabupaten_id'  => $data['kabupaten']
        ]);

        if ($request->hasFile('gambar')) {
            $files = $request->file('gambar');
            foreach ($files as $f) {
                $path = $f->store('iklan', 'uploads');
                Foto::create([
                    'iklan_id' => $iklan->id,
                    'foto'     => $path
                ]);
            }
        }

        return redirect('/iklan');
    }
}
