$(function () {
    $('.hapus-user-btn').click(function () {
        var form = $('.hapus-user form');
        form.attr('action', form.data('url') + '/' + $(this).data('id'));
    });

    $('.hapus-kategori-btn').click(function () {
        var form = $('.hapus-kategori form');
        form.attr('action', form.data('url') + '/' + $(this).data('id'));
    });

    $(".kategori-select").select2({
        placeholder: "Kategori Video"
    });

    $('ul.kategori').sortable({
        placeholder: "placeholder"
    });

    $('ul.sub-kategori').sortable({
        placeholder: "placeholder"
    });

    $('#kategori-urutan').submit(function (event) {
        var listKategori = $('.kategori li');
        var sorted = [];
        listKategori.each(function (i, e) {
            sorted.push($(e).data('id'));
        });

        $('.urutan').val(sorted.join(','));
    });
});