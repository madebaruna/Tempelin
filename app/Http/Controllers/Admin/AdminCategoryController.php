<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Psy\Util\Json;

class AdminCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    public function index()
    {
        $Category = [];
        $categories = Category::where('parent_id', NULL)->orderBy('urutan')->get();
        foreach ($categories as $c) {
            $cat = [
                'id' => $c->id,
                'nama' => $c->nama,
                'sub_category' => []
            ];

            $subs = Category::where('parent_id', $c->id)->orderBy('urutan')->get();
            foreach ($subs as $s) {
                $sub = [
                    'id' => $s->id,
                    'nama' => $s->nama
                ];

                array_push($cat['sub_category'], $sub);
            }

            array_push($Category, $cat);
        }

        return view('admin.kategori', [
            'kategori' => $Category
        ]);
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'icon' => 'required'
        ]);

        Category::create([
            'nama' => $request->nama,
            'slug' => str_slug($request->nama),
            'parent_id' => $request->parent == 0 ? NULL : $request->parent,
            'urutan' => Category::max('id') + 1,
            'icon' => $request->icon
        ]);

        return redirect()->back();
    }

    public function sort(Request $request)
    {
        $this->validate($request, [
            'urutan' => 'required'
        ]);

        $urutan = explode(',', $request->urutan);
        foreach ($urutan as $i => $k) {
            $Category = Category::find($k);
            $Category->urutan = $i;
            $Category->save();
        }

        return redirect()->back();
    }

    public function delete($id)
    {
        $kategori = Category::findOrFail($id);
        $kategori->delete();
        return redirect()->back();
    }
}
