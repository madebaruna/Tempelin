@extends('layout')

@section('title', $kategori->nama)

@section('content')
    <h2 class="kategori-title">Daftar Iklan</h2>
    <hr>
    <div class="row">
        @foreach($iklan as $i)
            <div class="col-md-4">
                <a href="/iklan/{{ $i->user_id }}/{{ $i->slug }}">
                    <div class="card">
                        <div class="card-image" style="background-image: url({{ $i->gambarPertama->foto }})">
                        </div>
                        <div class="card-content">
                            <h5>{{ $i->judul }}</h5>
                            <a class="kategori" href="/{{$i->kategori->slug}}">{{ $i->kategori->nama }}</a><br/>
                            <hr>
                            <span class="harga">{{ $i->harga() }}</span>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
@endsection