<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title') - {{ config('app.name') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Cabin:400,600|Raleway" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap-datetimepicker.min.css') }}">
</head>
<body>
<div class="container">
    <header>
        <a href="/"><img src="{{ asset('/img/logo-small.png') }}"></a>
        <div class="tombol-header pull-right">
            <p class="header-message hidden-xs hidden-sm">Tempel iklanmu disini!</p>
            @if(!Auth::user())
                <a href="/login" class="btn btn-default btn-lg"><i class="fa fa-sign-in"></i> Login</a>
            @else
                <div class="dropdown" style="display:inline-block;">
                    @php($notifications = \App\Notification::where('user_id', Auth::user()->id)->where('read', 0)->orderBy('id', 'desc')->get())
                    <button href="/login" class="btn btn-default btn-lg dropdown-toggle" type="button"
                            id="headerNotification"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <i class="fa fa-bell-o"></i> {{ $notifications->count() != 0 ? $notifications->count() : ''}}
                    </button>
                    <ul class="dropdown-menu notif-box" aria-labelledby="headerNotification" style="width: 250px;">
                        <li class="dropdown-header"
                            style="text-align: center; font-size: 16px; color: #222; padding-bottom: 0;">Notifikasi Anda
                        </li>
                        <li role="separator" class="divider"></li>
                        @if($notifications->count() == 0)
                            <li><a href="">Belum ada notifikasi</a></li>
                        @endif
                        @foreach($notifications as $notif)
                            <li>
                                <a href="/readnotif/{{ $notif->id }}">
                                    @if($notif->comment_id != null)
                                        @php($comment = \App\Comment::find($notif->comment_id))
                                        <span>
                                        @if($notif->read == 0)
                                                <span class="notif-new">baru</span>
                                            @endif
                                            Komentar di iklan anda</span><br/>
                                        {{ $comment->iklan->judul }}
                                    @else
                                        @php($pesan = \App\Pesan::find($notif->pesan_id))
                                        <span>
                                        @if($notif->read == 0)
                                                <span class="notif-new">baru</span>
                                            @endif
                                            Pesan baru dari</span><br/>
                                        {{ $pesan->user->name }}
                                    @endif
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="dropdown" style="display:inline-block;">
                    <button class="btn btn-default btn-lg dropdown-toggle" type="button" id="headerDropdown"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        {{ Auth::user()->name }}
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="headerDropdown">
                        @if(Auth::user()->isAdmin())
                            <li><a href="/admin"><i class="fa fa-dashboard"></i> Admin Dashboard</a></li>
                        @endif()
                        <li><a href="/iklan"><i class="fa fa-sticky-note"></i> Daftar Iklan</a></li>
                        <li><a href="/pesan"><i class="fa fa-envelope"></i> Pesan</a></li>
                        <li><a href="/profile"><i class="fa fa-user"></i> Profil</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/logout"><i class="fa fa-sign-out"></i> Logout</a></li>
                    </ul>
                </div>
            @endif
            <a href="/tempel" class="btn btn-primary btn-lg"><i class="fa fa-thumb-tack"></i> Tempel Iklan</a>
        </div>
        <div class="search">
            <div class="row no-gutter">
                <form role="form" method="POST" action="/cari">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="col-md-3 col-sm-12">
                            <div class="input-group kabupaten-select-search" data-toggle="modal"
                                 data-target=".pilih-kabupaten-search">
                                <span class="input-group-addon" id="kabupaten-addon"><i class="fa fa-globe"></i></span>
                                <input type="text" name="kabupaten" class="form-control input-lg kabupaten-input-search"
                                       placeholder="Kabupaten" aria-describedby="kabupaten-addon"
                                       value="{{ old('kabupaten-name') }}">
                            </div>
                            <input type="hidden" name="kabupaten" class="kabupaten-id-search" value="-1">
                            <input type="hidden" name="provinsi" class="provinsi-id-search" value="-1">
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="input-group kategori-select-search" data-toggle="modal"
                                 data-target=".pilih-kategori-search">
                                <span class="input-group-addon" id="kategori-addon"><i class="fa fa-list-ul"></i></span>
                                <input type="text" class="form-control input-lg kategori-input-search"
                                       placeholder="Kategori" aria-describedby="kategori-addon"
                                       value="{{ old('kategori-name') }}">
                            </div>
                            <input type="hidden" name="kategori" class="kategori-id-search"
                                   value="{{ old('kategori') }}">
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="input-group">
                                <input id="search-bar" type="text" class="form-control input-lg" name="search"
                                       value="{{ old('search') }}"
                                       autofocus placeholder="Cari apa yang kamu inginkan..."
                                       aria-describedby="search-addon">
                                <span class="input-group-btn">
                                    <button class="btn btn-lg btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </header>
    <section>
        @yield('content')
    </section>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <a href="">Pusat Bantuan</a>
                <a href="">Syarat & Ketentuan</a>
                <a href="">Tentang Tempelin</a>
                <a href="">Tips Belanja Aman</a>
                <span>Copyright 2017 Tempel.in</span>
            </div>
            <div class="col-md-6" style="text-align: left;">
                <img src="{{ asset('/img/logo-invert.png') }}">
            </div>
        </div>


    </div>
</footer>

@include('pilih_kategori_search')
@include('pilih_kabupaten_search')

<script src="{{ asset('/js/jquery.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/select2.min.js') }}"></script>
<script src="{{ asset('/js/app.js') }}"></script>
<script src="{{ asset('/js/moment-with-locales.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('/js/auth.js') }}"></script>
</body>
</html>