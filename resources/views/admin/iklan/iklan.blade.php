@extends('admin.layout')

@section('title', 'Iklan')
@section('title-header', 'Daftar Iklan')

@section('active-iklan', 'active')

@section('content')
    <h4>Iklan Belum Diverifikasi</h4>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>ID</th>
            <th>Judul</th>
            <th>User</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @if($unverifiedIklan->count() == 0)
            <tr>
                <td colspan="4">Semua iklan sudah terverifikasi!</td>
            </tr>
        @endif
        @foreach($unverifiedIklan as $u)
            <tr>
                <td class="fit">{{ $u->id }}</td>
                <td>{{ $u->judul }}</td>
                <td>{{ $u->user->name }}</td>
                <td class="fit">
                    <a class="btn btn-xs btn-primary" href="/admin/iklan/{{ $u->id }}">
                        <i class="fa fa-eye"></i> Lihat
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <hr>
    <h4>Iklan Terverifikasi</h4>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>ID</th>
            <th>Judul</th>
            <th>User</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($verifiedIklan as $u)
            <tr>
                <td class="fit">{{ $u->id }}</td>
                <td>{{ $u->judul }}</td>
                <td>{{ $u->user->name }}</td>
                <td class="fit">
                    <a class="btn btn-xs btn-primary" href="/admin/iklan/{{ $u->id }}">
                        <i class="fa fa-eye"></i> Lihat
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $verifiedIklan->links() }}
@endsection