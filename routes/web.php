<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index');

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/readnotif/{id}', 'NotificationController@read');
Route::post('/cari', 'SearchController@searchRedirect');
Route::get('/cari/{provinsi}/{kabupaten}/{kategori}/{search}', 'SearchController@search');
Route::get('/cari/{provinsi}/{kabupaten}/{kategori}', 'SearchController@search');
Route::get('/laku/{id}', 'FormIklanController@laku');

Route::group(['prefix' => 'pesan'], function () {
    Route::get('/', 'PesanController@get');
    Route::get('/kirim/{p}', 'PesanController@sendNew');
    Route::get('/{penerima}', 'PesanController@show');
    Route::post('/{p}', 'PesanController@send');
});

Route::group(['prefix' => 'tempel'], function () {
    Route::get('/', 'FormIklanController@add');
    Route::post('/', 'FormIklanController@post');
});

Route::group(['prefix' => 'profile'], function () {
    Route::get('/', 'UserProfileController@profile')->middleware('auth');
    Route::get('/edit', 'UserProfileController@edit');
    Route::post('/update', 'UserProfileController@update');
    Route::get('/{id}', 'UserProfileController@profile');
});

Route::group(['prefix' => 'iklan'], function () {
    Route::get('/', 'IklanController@get');
    Route::get('/lokasi/{id}', 'IklanController@lokasi');
    Route::get('/{user_id}/{slug}', 'IklanController@detail');
});

Route::group(['prefix' => 'komentar'], function () {
    Route::post('/{id}', 'KomentarController@kirim');
});

/*
 * Route untuk bagian admin
 */
Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'Admin\AdminController@index');

    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'Admin\AdminUserController@index');
        Route::post('/delete/{id}', 'Admin\AdminUserController@delete');
    });

    Route::group(['prefix' => 'kategori'], function () {
        Route::get('/', 'Admin\AdminCategoryController@index');
        Route::post('/add', 'Admin\AdminCategoryController@add');
        Route::post('/delete/{id}', 'Admin\AdminCategoryController@delete');
        Route::post('/urut', 'Admin\AdminCategoryController@sort');
    });

    Route::group(['prefix' => 'iklan'], function () {
        Route::get('/', 'Admin\AdminIklanController@index');
        Route::get('/delete/{id}', 'Admin\AdminIklanController@delete');
        Route::get('/verifikasi/{id}', 'Admin\AdminIklanController@verify');
        Route::get('/{id}', 'Admin\AdminIklanController@detail');
    });
});

//listing iklan sesuai url
Route::get('/{slug}', 'IklanController@listing');