<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Helpers\NotificationHelper;
use App\Iklan;
use App\Kabupaten;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KomentarController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function kirim($id, Request $request)
    {
        $this->validate($request, [
            'isi' => 'string|required'
        ]);

        $iklan = Iklan::findOrFail($id);

        $komentar = Comment::create([
            'user_id'  => Auth::user()->id,
            'isi'      => $request->isi,
            'iklan_id' => $iklan->id,
        ]);

        NotificationHelper::kirim('komentar', $iklan->user, $komentar);

        return redirect()->back();
    }
}
