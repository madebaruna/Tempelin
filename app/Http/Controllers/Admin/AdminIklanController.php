<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Iklan;
use App\User;

class AdminIklanController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    public function index()
    {
        $verifiedIklan = Iklan::where('isVerified', '1')->orderBy('id', 'desc')->paginate();
        $unverifiedIklan = Iklan::where('isVerified', '0')->get();
        return view('admin.iklan.iklan', [
            'verifiedIklan' => $verifiedIklan,
            'unverifiedIklan' => $unverifiedIklan
        ]);
    }

    public function detail($id)
    {
        $iklan = Iklan::findOrFail($id);
        $gambar = $iklan->gambar;
        return view('admin.iklan.iklan_detail', [
            'iklan' => $iklan,
            'gambar' => $gambar
        ]);
    }

    public function verify($id)
    {
        $iklan = Iklan::findOrFail($id);
        $iklan->verify();
        return redirect('/admin/iklan');
    }

    public function delete($id)
    {
        $iklan = Iklan::findOrFail($id);
        $iklan->delete();
        return redirect('/admin/iklan');
    }
}
