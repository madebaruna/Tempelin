@extends('layout')

@section('title', $iklan->judul)

@section('content')
    <h2 class="kategori-title">{{ $iklan->judul }}</h2>
    @if($iklan->status == 'laku')
        <h4 style="text-align: center; color: #1abc9c;"><i class="fa fa-check"></i> Sudah Laku</h4>
    @endif
    <div class="detail-iklan">
        <i class="fa fa-map-marker"></i>
        {{ $iklan->kabupaten->kabupaten }},
        {{ $iklan->provinsi->provinsi }}

        <i style="display: inline-block; margin-left: 10px;" class="fa fa-clock-o"></i>
        {{ (new \Carbon\Carbon($iklan->created_at))->format('d M Y H:m') }}
    </div>
    <hr>
    <div class="row">
        <div class="col-md-8 iklan-left-col">
            <div class="foto">
                <a href="{{ $iklan->gambarPertama->foto }}" target="_blank" id="gambar-iklan-link">
                    <div class="gambar-iklan-box">
                        <img src="{{ $iklan->gambarPertama->foto }}" id="gambar-iklan">
                    </div>
                </a>
                <div class="thumb">
                    @foreach($iklan->gambar as $k => $g)
                        <img class="{{ $k==0 ? 'selected' : '' }}" src="{{ $g->foto }}">
                    @endforeach
                </div>
            </div>
            <div class="deskripsi">
                <p>
                    {!! nl2br(e($iklan->deskripsi)) !!}
                </p>
            </div>
            <hr>
            <div class="komentar">
                <form action="/komentar/{{ $iklan->id }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="isi-komentar">Komentar</label>
                        <textarea class="form-control" id="isi-komentar" name="isi" rows="3"></textarea>
                        <button class="btn btn-primary kirim">Kirim</button>
                    </div>
                </form>
            </div>
            <div class="komentar-isi">
                <ul>
                    @foreach($komentar as $k)
                        <li>
                            <img src="{{ $k->user->foto() }}">
                            <a href="/profile/{{ $k->user->id }}" class="nama">{{ $k->user->name }}</a>
                            <span class="isi">{{ $k->isi }}</span>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-md-4 iklan-right-col">
            <div class="harga">{{ $iklan->harga() }}</div>
            @if(Auth::user() != null)
            @if(Auth::user()->id != $iklan->user_id && $iklan->status != 'laku')
                <a href="/pesan/kirim/{{ $user->id }}" class="btn btn-primary beli" style="display: block;">BELI</a>
            @endif
            @endif
            <div class="box profile">
                <div class="gambar"
                     style="background-image:url({{ $user->foto() }});">
                </div>
                <a href="/profile/{{ $user->id }}"><h4>{{ $user->name }}</h4></a>
                <p>Member sejak {{ (new \Carbon\Carbon($user->created_at))->format('M Y') }}</p>
            </div>
            <div class="box telepon">
                <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-phone fa-stack-1x fa-inverse"></i>
                </span>
                <h4>{{ $iklan->nomor_telepon }}</h4>
            </div>
            @if(Auth::user() != null)
            @if(Auth::user()->id != $iklan->user_id && $iklan->status != 'laku')
                <a href="/pesan/kirim/{{ $user->id }}" class="btn btn-primary pesan" style="display: block;">
                    <i class="fa fa-envelope"></i>
                    Kirim Pesan
                </a>
            @elseif(Auth::user()->id == $iklan->user_id && $iklan->status != 'laku')
                <a href="/laku/{{ $iklan->id }}" class="btn btn-warning pesan" style="display: block;">
                    <i class="fa fa-check"></i>
                    Tandai Sudah Laku
                </a>
            @endif
            @endif
        </div>
    </div>
@endsection