<div class="modal fade pilih-kategori-search" tabindex="-1" role="dialog" aria-labelledby="pilih-kategori">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Kategori</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 main-category-box">
                        <ul class="nav nav-pills nav-stacked">
                            @php(
                                $category = \App\Category::getCategory()
                            )
                            @foreach($category as $c)
                                <li role="presentation" class="main-category-li">
                                    <a class="main-category" data-id="{{ $c['id'] }}" href="#">
                                        <i class="fa fa-{{ $c['icon'] }}"></i> {{ $c['nama'] }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-md-6 sub-category-box">
                        <i class="main-category-icon"></i>
                        <span class="main-category-name"></span>
                        <ul class="nav nav-pills nav-stacked sub-category-list-search">
                        </ul>
                    </div>
                </div>
                <input type="hidden" class="category-data" value="{{ json_encode($category) }}">
            </div>
        </div>
    </div>
</div>
</div>
